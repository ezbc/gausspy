.. _install:

===============
Installation
===============


------------
 Dependencies
------------

*  `Python 2.7 <http://www.numpy.org/>`_

* `Numpy <http://www.numpy.org/>`_

* `Scipy <http://www.scipy.org/>`_

* `h5py <http://www.h5py.org/>`_

* `GNU Scientific Library (GSL) <http://www.gnu.org/software/gsl/>`_


If you do not already have Python 2.7, you can
install the 
`Anaconda Scientific Python distribution <https://store.continuum.io/cshop/anaconda/>`_, 
which comes pre-loaded with Numpy, Scipy, and h5py.

To obtain GSL:

.. code-block:: none

   sudo apt-get install libgsl0-dev


---------------
Download GaussPy
---------------

Download GaussPy from...


------------
Installing GaussPy
------------

After install all dependencies, 
you can install GaussPy via

.. code-block:: python
   tar -xvf 
   python setup.py install
